#include <iostream>
#include "depthai/depthai.hpp"
#include <opencv2/opencv.hpp>

// #include <depthai-shared/common/CameraBoardSocket.hpp>

// #include <depthai/pipeline/node/ColorCamera.hpp>

int64_t last_frame_seq_r;
int64_t this_frame_seq_r;
int64_t last_frame_seq_l;
int64_t this_frame_seq_l;

int main(int argc, char **argv) {

  dai::Pipeline p;

  for (dai::CameraBoardSocket socket :
       {dai::CameraBoardSocket::LEFT, dai::CameraBoardSocket::RIGHT}) {
    auto xlinkOut = p.create<dai::node::XLinkOut>();
    char stream_name[64];
    sprintf(stream_name, "cam%d", socket);
    xlinkOut->setStreamName(stream_name);
    auto colorCam = p.create<dai::node::ColorCamera>();

    colorCam = p.create<dai::node::ColorCamera>();
    colorCam->setBoardSocket(socket);
    colorCam->setPreviewSize(1280, 800);
    colorCam->setResolution(dai::ColorCameraProperties::SensorResolution::THE_800_P);
    colorCam->setImageOrientation(dai::CameraImageOrientation::NORMAL);
    colorCam->setInterleaved(true);
    colorCam->setFps(60);
    colorCam->setColorOrder(dai::ColorCameraProperties::ColorOrder::BGR);

    // Link plugins CAM -> XLINK
    colorCam->preview.link(xlinkOut->input);
  }

  dai::Device device(p);

  auto queue_left =
      device.getOutputQueue("cam1", 1, true).get(); // out of shared pointer
  auto queue_right = device.getOutputQueue("cam2", 1, true).get();

  while (true) {
    std::shared_ptr<dai::ImgFrame>
        depthai_frame_right; //= depthai->queue_right->get<dai::ImgFrame>();
    std::shared_ptr<dai::ImgFrame>
        depthai_frame_left; // = depthai->queue_left->get<dai::ImgFrame>();

    while (!(depthai_frame_right && depthai_frame_left)) {
      auto ev = device.getQueueEvent();
      // U_LOG_E("%s", ev.c_str());
      if (ev == "cam1") {
        depthai_frame_left = queue_left->get<dai::ImgFrame>();
        last_frame_seq_l = depthai_frame_left->getSequenceNum();

      } else if (ev == "cam2") {
        depthai_frame_right = queue_right->get<dai::ImgFrame>();
        last_frame_seq_r = depthai_frame_right->getSequenceNum();
      }
    }

    printf("l %li r %li", last_frame_seq_l, last_frame_seq_r);
    cv::Mat cvFrame_left =
        depthai_frame_right
            ->getCvFrame(); // you need to build depthai with OpenCV support.
    cv::Mat cvFrame_right = depthai_frame_left->getCvFrame();

    cv::imshow("left", cvFrame_left);
    cv::imshow("right", cvFrame_right);
    cv::waitKey(1);
  }

  return 0;
}

// dai::Pipeline createCameraPipeline() {
//     dai::Pipeline p;

//     auto colorCam = p.create<dai::node::ColorCamera>();
//     auto xlinkOut = p.create<dai::node::XLinkOut>();
//     xlinkOut->setStreamName("preview");

//     colorCam->setPreviewSize(300, 300);
//     colorCam->setResolution(dai::ColorCameraProperties::SensorResolution::THE_1080_P);
//     colorCam->setInterleaved(true);

//     // Link plugins CAM -> XLINK
//     colorCam->preview.link(xlinkOut->input);

//     return p;
// }

// int main() {
//     using namespace std;

//     dai::Pipeline p = createCameraPipeline();

//     // Start the pipeline
//     dai::Device d;

//     cout << "Connected cameras: ";
//     for(const auto& cam : d.getConnectedCameras()) {
//         cout << static_cast<int>(cam) << " ";
//     }
//     cout << endl;

//     // Start the pipeline
//     d.startPipeline(p);

//     cv::Mat frame;
//     auto preview = d.getOutputQueue("preview");

//     while(1) {
//         auto imgFrame = preview->get<dai::ImgFrame>();
//         if(imgFrame) {
//             printf("Frame - w: %d, h: %d\n", imgFrame->getWidth(), imgFrame->getHeight());
//             frame = cv::Mat(imgFrame->getHeight(), imgFrame->getWidth(), CV_8UC3, imgFrame->getData().data());
//             cv::imshow("preview", frame);
//             int key = cv::waitKey(1);
//             if(key == 'q') {
//                 return 0;
//             }
//         } else {
//             std::cout << "Not ImgFrame" << std::endl;
//         }
//     }

//     return 0;
// }
